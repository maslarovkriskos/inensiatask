﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MovieStarTask
{
    class Writer
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ShowResult(ReadTextFile("C:\\Users\\ASUS\\source\\repos\\input.txt")));
        }

        public static List<MovieStar> ReadTextFile(string path)
        {
            var json = File.ReadAllText(path);

            var customers = JsonConvert.DeserializeObject<List<MovieStar>>(json);

            return customers;
        }

        public static string ShowResult(List<MovieStar> stars)
        {
            stars = ReadTextFile("C:\\Users\\ASUS\\source\\repos\\input.txt");

            var sb = new StringBuilder();

            foreach (var item in stars)
            {
                var year = item.DateOfBirth.TakeLast(4).ToArray();

                var age = string.Empty;

                for (int i = 0; i < year.Length; i++)
                {
                    age += year[i];
                }

                var currentAge = DateTime.Now.Year - int.Parse(age);

                sb.AppendLine($"{item.Name} {item.Surname}");
                sb.AppendLine($"{item.Sex}");
                sb.AppendLine($"{item.Nationality}");
                sb.AppendLine($"{currentAge} years old");
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
