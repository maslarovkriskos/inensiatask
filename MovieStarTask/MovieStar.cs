﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieStarTask
{
    public class MovieStar
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Sex { get; set; }
        public string Nationality { get; set; }
        public string DateOfBirth { get; set; }
    }
}
