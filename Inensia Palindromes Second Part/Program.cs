﻿using System;

namespace Inensia_Palindromes_Second_Part
{
    class Program
    {
        static void Main(string[] args)
        {
            //I made it to check the word from the task. It can be done to check whatever you want if its stored in a variable:
            //string word = Console.ReadLine();
            //Console.WriteLine(IsPalindrome(word));

            Console.WriteLine(IsPalindrome("race car"));
        }

        public static bool IsPalindrome(string myString)
        {
            myString = myString.Replace(" ", "");

            string first = myString.Substring(0, myString.Length / 2);
            char[] arr = myString.ToCharArray();

            Array.Reverse(arr);

            string temp = new string(arr);
            string second = temp.Substring(0, temp.Length / 2);

            if (first.Equals(second))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
